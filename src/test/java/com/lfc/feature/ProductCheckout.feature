#Author: Sejal Jawale

Feature: Testing Registration Page

  @SmokeTest2
  Scenario: LFC_RP_01 Verify that the user is able to register using Registration form
    Given user is on registration page
    Then enter firstname, surname, email, DOB
    And create password,re-enter the password
    And click on create an account button

  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with <name>
    When I check for the <value> in step
    Then I verify the <status> in step

    Examples:
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
