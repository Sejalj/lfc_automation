package stepDefinition;

import static org.testng.Assert.assertTrue;

import org.testng.Assert;

import com.lfc.pageObjects.AccountPage;
import com.lfc.pageObjects.HomePage;
import com.lfc.pageObjects.LoginPage;
import com.lfc.utils.BaseClass;
import com.lfc.utils.Log;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps extends BaseClass {

	private static final String Sign = null;
	LoginPage loginPage = new LoginPage(driver);
	Log log= new Log();
	HomePage homePage= new HomePage(driver);
	AccountPage accountPage= new AccountPage(driver);
	CommonSteps commonSD= new CommonSteps();


	@And("^click on \"([^\"]*)\" button$")
	public void Click_on_valid_button(String button) throws Throwable {
		try {
			switch (button) {
				case "Sign In":
					loginPage.clickOnSignInButton();
					break;

				case "Register for free" :
					loginPage.clickOnRegisterForFreeButton();
					break;

				default: break;
			}
			commonSD.takeScreenshot();
			Log.info("Click on valid button");
		}
		catch(Exception e) {
			Assert.fail("Unable to click on the button, Exception: " + e.getMessage());
		}
	}

	@Given("^user is on login screen$")
	public void User_on_login_screen() throws Throwable {
		try{
			homePage.closeCookieAlert();
			loginPage.clickOnAccountIconLink();
			commonSD.takeScreenshot();

			Log.info("Clicking on Account Icon link is successful");
		}
		catch(Exception e) {
			Assert.fail("Unable to login on the login screen., Exception: " + e.getMessage());
		}

	}

	@When("^enter email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void Enter_valid_credential(String username, String password) throws Throwable {
		try {
			loginPage.loginIntoApplication(username, password);
			commonSD.takeScreenshot();
			Log.info("Login into application is successful");
		}
		catch(Exception e) {
			Assert.fail("Unable to enter email and password, Exception: " + e.getMessage());
		}

	}

	@And("^logout from the application$")
	public void Logout_from_application() throws Throwable {
		try {
			accountPage.logoutFromApplication();
			commonSD.takeScreenshot();
			Log.info("Logout from the application is successful");
		}
		catch(Exception e) {
			Assert.fail("Unable to logout from the application, Exception: " + e.getMessage());
		}

	}

	@Then("^verify that incorrect username and password error message is displayed$")
	public void Verify_that_incorrect_username_and_password_error_message_is_displayed() throws Throwable {
		try {

			boolean errorMessage = loginPage.verifyIncorrectUsernamePasswordErrorisDisplayed();
			commonSD.takeScreenshot();
			assertTrue(errorMessage, "Incorrect Username and Password Error message should be displayed.");

			Log.info("verify that incorrect username and password error message is displayed");
		}
		catch(Exception e) {
			Assert.fail("Incorrect username and password message not displayed, Exception: " + e.getMessage());
		}

	}

	@Then("^verify that email field error message is displayed$")
	public void Verify_that_emai_field_error_message_is_displayed() throws Throwable {
		try {

			boolean errorMessage = loginPage.verifyEmailFieldErrorIsDisplayed();
			String actualErrorMessage = loginPage.getEmailFieldErrorMessage();
			String expectedErrorMessage = "Please enter a valid email address (Ex: johndoe@domain.com).";

			if(expectedErrorMessage.equalsIgnoreCase(actualErrorMessage))
				System.out.println("Email field error message expected is same as displayed error --- "+actualErrorMessage);
			else
				System.out.println("Email field error message doesn't match the displayed error --- "+actualErrorMessage);

			commonSD.takeScreenshot();
			assertTrue(errorMessage, "Email field error message should be displayed.");

			Log.info("verify that email field error message is displayed");
		}
		catch(Exception e) {
			Assert.fail("Email Field message not displayed, Exception: " + e.getMessage());
		}

	}

	@Then("^verify that email and password required error message is displayed$")
	public void Verify_that_email_password_required_error_message_is_displayed() throws Throwable {
		try {

			boolean emailErrorMessage = loginPage.verifyEmaildRequiredErrorIsDisplayed();
			boolean passwordErrorMessage = loginPage.verifyPasswordRequiredErrorIsDisplayed();

			if (emailErrorMessage && passwordErrorMessage == true) {
				assertTrue(emailErrorMessage , "Email required error message should be displayed.");
				assertTrue(passwordErrorMessage , "Password required error message should be displayed.");

				Log.info("verify that email and password required error message is displayed ");
			} else {
				System.out.println("Email & Password required error message was not displayed.");
			}
			commonSD.takeScreenshot();
		}
		catch(Exception e) {
			Assert.fail("Email and Password required error message not displayed, Exception: " + e.getMessage());
		}

	}





}
