#Author: Sejal Jawale

Feature: AddToCart product

  @SmokeTest1
  Scenario: Add product into cart as signed-in user
    Given user logged-in into application using email "User14012022130020@gmail.com" and password "Password@14012022130020"
    And navigate to product url
    Then select product size
    And  add product to bag
    Then verify the product is added to bag

  @Regression
  Scenario: Add product into cart as guest user
    Given user logged-in into application using email "User14012022130020@gmail.com" and password "Password@14012022130020"
    When select the product
    And checkout the product
    Then verify the payment confirmation
    And  logout from the application