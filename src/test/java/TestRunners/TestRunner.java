package TestRunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
//import io.cucumber.junit.CucumberOptions;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features= {"src/test/java/com/lfc/feature"}
        , monochrome = true
		, plugin = {"pretty" ,"html:target/cucumber-reports/cucumber" ,
		  "json:target/cucumber-reports/cucumber.json" ,
		  "junit:target/cucumber-reports/cucumber.xml",
		  "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"}
        , glue = {"stepDefinition"}
        , dryRun= false
        , tags = "@SmokeTest1"
        //, tags = "@Regression"
)

public class TestRunner extends AbstractTestNGCucumberTests
{
	
}

