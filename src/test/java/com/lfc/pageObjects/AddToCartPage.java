package com.lfc.pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.lfc.utils.BaseClass;

public class AddToCartPage extends BaseClass {

// ==================Add to Cart page locators=======================

	@FindBy(id = "product-addtocart-button")
	private WebElement addToCartButton;

	@FindBy(id = "option-label-size-205-item-191")
	private WebElement size8Option;

	@FindBy(className = "product-item-name")
	private WebElement productTitleInMiniCart;

	@FindBy(linkText = "Remove")
	private WebElement removeButtonInMiniCart;

	@FindBy(id = "top-cart-btn-checkout")
	private WebElement proceedToCheckoutButtonAtMiniCart;

	@FindBy(className = "action-primary action-accept")
	private WebElement removeItemConfirmationOptionOk;

	@FindBy(xpath = "//button[@name='processAddress']")
	private WebElement proceedToCheckoutButtonAtShoppingCartAddress;

	@FindBy(xpath = "//div[@class='checker']/span/input[@type='checkbox']")
	private WebElement termsAndConditionCheckbox;

	@FindBy(xpath = "//button[@name='processCarrier']")
	private WebElement proceedToCheckoutButtonAtShoppingCartShipping;

	@FindBy(xpath = "//a[@class='bankwire']")
	private WebElement bankWirePaymentButton;

	@FindBy(xpath = "//button[@type='submit']/span[contains(text(),'I confirm my order')]")
	private WebElement confirmOrderButton;

	@FindBy(xpath = "/p[@class='cheque-indent']/strong[contains(text(),'Your order on My Store is complete.')]")
	private WebElement orderConfirmationMessage;



//=========================================================================

	public AddToCartPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void navigationToProductURL()
	{
		driver.navigate().to("https://store.liverpoolfc.com/lfc-womens-ynwa-black-cropped-funnel-neck-jacket");
	}

	public void clickOnSize8Option()
	{
		size8Option.click();
	}

	public void clickOnAddToCartButton()
	{
		addToCartButton.click();
	}

	public String getProductTitleFromMiniCart() throws InterruptedException {
		return productTitleInMiniCart.getText();
	}

	public void clickOnRemoveButtonInMiniCart() throws InterruptedException {
		removeButtonInMiniCart.click();
	}

	public void clickOnRemoveConfirmationOptionOk() throws InterruptedException {
		removeItemConfirmationOptionOk.click();
	}

	public void clickOnProceedToCheckoutButton() throws InterruptedException {
		//proceedToCheckoutButton.click();
	}


	public void clickOnProceedToCheckoutButtonAtMiniCartButton() throws InterruptedException {
		proceedToCheckoutButtonAtMiniCart.click();
	}


	public void clickOnProceedToCheckoutButtonAtShoppingCartAddressButton() throws InterruptedException {
		proceedToCheckoutButtonAtShoppingCartAddress.click();
	}

	public void checkTermsAndConditionCheckbox() throws InterruptedException {
		termsAndConditionCheckbox.click();
	}


	public void clickOnProceedToCheckoutButtonAtShoppingCartShippingButton() throws InterruptedException {
		proceedToCheckoutButtonAtShoppingCartShipping.click();
	}

	public void clickOnBankWirePaymentButton() throws InterruptedException {
		bankWirePaymentButton.click();
	}


	public void clickOnConfirmOrderButton() throws InterruptedException {
		confirmOrderButton.click();
	}


	public String getOrderConfirmationMessage()
	{
		return orderConfirmationMessage.getText();
	}

}
