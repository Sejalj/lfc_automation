package com.lfc.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.lfc.utils.BaseClass;

public class AccountPage extends BaseClass {

// ==================Account page locators=======================

    @FindBy(xpath = "//header/div[1]/div[1]/div[4]/div[2]/div[1]/ul[1]/li[1]/span[1]")
    private WebElement profileIcon;

    @FindBy(xpath = "//a[contains(text(),'Sign Out')]")
    private WebElement signOut;

//=========================================================================

    public AccountPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void clickOnProfileIconLink() throws InterruptedException {
        Thread.sleep(4000);
        profileIcon.click();
    }

    public void logoutFromApplication() {
        signOut.click();
    }

    public boolean verifySignOutisDisplayed() throws InterruptedException{
        clickOnProfileIconLink();
        return signOut.isDisplayed();
    }

}
