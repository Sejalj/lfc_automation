package com.lfc.pageObjects;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.lfc.utils.BaseClass;

public class PLPPage extends BaseClass {

// ==================PLP page locators=======================

	@FindBy(xpath = "a[@title='Faded Short Sleeve T-shirts' and @class='product-name']")
	private WebElement fadedShortTshirtLink;

	@FindBy(className = "page-title")
	private WebElement productTitle;

	@FindBy(className = "dy-lb-close")
	private WebElement popUpCloseButton;

	@FindBy(className = "_hj-OO1S1__styles__openStateToggle")
	private WebElement feedbackBoxCloseButton;

	@FindBy(id = "minicart-desktop")
	private WebElement cartIcon;




//=========================================================================

	public PLPPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void clickOnFadedShortTshirtLink() throws InterruptedException {
		fadedShortTshirtLink.click();
	}

	public String getProductTitle() throws InterruptedException {
		return productTitle.getText();
	}

	public void clickOnPopUpCloseButton() throws InterruptedException {
		Thread.sleep(4000);
		popUpCloseButton.click();

	}

	public void clickOnCartIcon() throws InterruptedException {
		cartIcon.click();

	}


}
