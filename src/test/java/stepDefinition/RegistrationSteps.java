package stepDefinition;

import com.lfc.pageObjects.HomePage;
import com.lfc.pageObjects.LoginPage;
import com.lfc.pageObjects.RegistrationPage;
import com.lfc.utils.BaseClass;
import com.lfc.utils.Log;
import com.lfc.utils.PropertyReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;


public class RegistrationSteps extends BaseClass {

    RegistrationPage registrationPage = new RegistrationPage(driver);
    LoginPage loginPage = new LoginPage(driver);
    HomePage homePage= new HomePage(driver);
    public PropertyReader propertyReader = new PropertyReader();

    @Given("^user is on registration page$")
    public void UserOnregistrationScreen() throws Throwable {
        try{
            homePage.closeCookieAlert();
            homePage.clickOnJoinButton();
            loginPage.clickOnRegisterForFreeButton();

            Log.info("user is on registration page is successful");
        }
        catch(Exception e) {
            Assert.fail("User is unable to register., Exception: " + e.getMessage());
        }


    }

    @Then("^enter firstname, surname, email, DOB$")
    public void EnterValidFirstNameAndSurname() throws Throwable {
        try {
            registrationPage.enterTitle("Miss");
            registrationPage.enterFirstName("Test");
            registrationPage.enterSurName("Automation");
            registrationPage.enterDOB("10/11/1990");
            String newEmailID= registrationPage.generateNewEmailId();
            System.out.println("New Email-ID created is " +newEmailID);
            registrationPage.enterNewEmailId(newEmailID);

            Log.info("enter firstname, surname, email, DOB is successful");
        }
        catch(Exception e) {
            Assert.fail("User is unable to enter firstname surname email and DOB., Exception: " + e.getMessage());
        }


    }

    @And("^create password,re-enter the password$")
    public void CreateNewPasswordAndReEnterIt() throws Throwable {

        try{
            String newPassword = registrationPage.generateNewPassword();
            System.out.println("New Password created is " +newPassword);
            registrationPage.enterNewPassword(newPassword);
            registrationPage.enterReEnterNewPassword(newPassword);
            Log.info("create password and re-enter the password is successful");
        }
        catch(Exception e) {
            Assert.fail("User is unable to create password and confirm it., Exception: " + e.getMessage());
        }


    }

    @And("^click on create an account button$")
    public void ClickOnCreateAnAccountButton() throws Throwable {

        try{
            registrationPage.clickOnCreateAnAccountButton();

            Log.info("Click On Create an Account Button is successful");
        }
        catch(Exception e) {
            Assert.fail("User is unable to click on create an account button., Exception: " + e.getMessage());
        }


    }
}

