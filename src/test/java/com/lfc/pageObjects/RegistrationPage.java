package com.lfc.pageObjects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.lfc.utils.BaseClass;

public class RegistrationPage extends BaseClass {

// ==================Registration page locators=======================

    @FindBy(xpath = "//input[@id='prefix']")
    private WebElement titleField;

    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstNameField;

    @FindBy(xpath = "//input[@id='lastname']")
    private WebElement surnameField;

    @FindBy(xpath = "//input[@id='email_address']")
    private WebElement emailIdField;

    @FindBy(xpath = "//input[@id='dob']")
    private WebElement dobField;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement newPasswordField;

    @FindBy(xpath = "//input[@id='password-confirmation']")
    private WebElement reenterPasswordField;

    @FindBy(xpath = "//select[@id='gender']")
    private WebElement genderDropDown;

    @FindBy(xpath = "//body/div[2]/main[1]/div[3]/div[1]/form[1]/div[1]/div[1]/button[1]")
    private WebElement createAnAccountButton;
//=========================================================================

    public RegistrationPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String generateNewEmailId () {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String userName= dateFormat.format(date);
        String emailID = "User"+userName+"@gmail.com";
        return emailID;
    }

    public String generateNewPassword () {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String randomString= dateFormat.format(date);
        String newPassword = "Password@"+randomString;
        return newPassword;
    }

    public void enterTitle(String title) {
        titleField.clear();
        titleField.sendKeys(title);
    }

    public void enterFirstName(String firstname) {
        firstNameField.clear();
        firstNameField.sendKeys(firstname);
    }

    public void enterSurName(String Surname) {
        surnameField.clear();
        surnameField.sendKeys(Surname);
    }

    public void enterNewEmailId(String newEmailId) {
        emailIdField.clear();
        emailIdField.sendKeys(newEmailId);
    }

    public void enterNewPassword(String newPassword) {
        newPasswordField.clear();
        newPasswordField.sendKeys(newPassword);
    }

    public void enterReEnterNewPassword(String reenterPassword) {
        reenterPasswordField.clear();
        reenterPasswordField.sendKeys(reenterPassword);
    }

    public void enterDOB(String dob) {
        dobField.clear();
        dobField.sendKeys(dob);
    }

    public void clickOnCreateAnAccountButton() {
        createAnAccountButton.click();
    }


}
