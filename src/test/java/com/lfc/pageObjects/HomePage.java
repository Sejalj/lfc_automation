package com.lfc.pageObjects;

import com.lfc.utils.BaseClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BaseClass {

// ==================Home page locators=======================

	@FindBy(id = "onetrust-banner-sdk")
	private WebElement cookiesAlert;

	@FindBy(id = "onetrust-accept-btn-handler")
	private WebElement acceptAllCookiesButton;

	@FindBy(className = "css-1hi3jcq")
	private WebElement join;

	@FindBy(xpath = "//a[contains(text(),'Sign In')]")
	private WebElement accountIcon;


//=========================================================================

	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void closeCookieAlert() throws Exception {
		Thread.sleep(4000);
		if (cookiesAlert.isDisplayed()) {
			acceptAllCookiesButton.click();
		};
	}

	public void clickOnJoinButton() {
		accountIcon.click();
	}



}
