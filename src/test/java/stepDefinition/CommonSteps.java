package stepDefinition;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.lfc.utils.BaseClass;

public class CommonSteps extends BaseClass {

    public void takeScreenshot() {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
            Date date = new Date();
            String fileName= dateFormat.format(date);
            FileUtils.copyFile(screenshot, new File("/Users/sejaljawale/Downloads/LFC_Automation/screenshots/screenshot_" + fileName));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}