package com.lfc.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.lfc.utils.BaseClass;

public class LoginPage extends BaseClass {



// ==================Login page locators=======================

	@FindBy(xpath = "//a[contains(text(),'Sign In')]")
	private WebElement accountIcon;

	@FindBy(id = "email")
	private WebElement emailAddress;

	@FindBy(name = "login[password]")
	private WebElement password;

	@FindBy(id = "send2")
	private WebElement SignInButton;

	@FindBy(linkText = "Register for free")
	private WebElement registerforFreeButton;

	@FindBy(id = "email_create")
	private WebElement createAccountEmailAddress;

	@FindBy(name = "SubmitCreate")
	private WebElement createAccountButton;

	@FindBy(id = "create_account_error")
	private WebElement createAccountValidationMsg;

	@FindBy(xpath = "//span[contains(text(),'Register for free')]")
	private WebElement registerForFreeButton;

	@FindBy(xpath = "//div[contains(text(),'Incorrect username or password.')]")
	private WebElement incorrectUsernamePasswordError;

	@FindBy(xpath = "//div[@id='email-error']")
	private WebElement emailFieldErrorMessage;

	@FindBy(xpath = "//div[@id='email-error']")
	private WebElement emailRequiredFieldErrorMessage;

	@FindBy(xpath = "//div[@id='pass-error']")
	private WebElement passwordRequiredFieldErrorMessage;

//=========================================================================


	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void clickOnAccountIconLink() {
		accountIcon.click();
	}

	public void enterEmailAddress(String email) {
		emailAddress.clear();
		emailAddress.sendKeys(email);
	}

	public void enterPassword(String pass) {
		password.clear();
		password.sendKeys(pass);
	}

	public void clickOnSignInButton() {
		SignInButton.click();
	}

	public void loginIntoApplication(String email, String pass) {
		clickOnAccountIconLink();
		enterEmailAddress(email);
		enterPassword(pass);
		clickOnSignInButton();
	}

	public void enterCreateAccountEmail(String createEmailAddress) {
		createAccountEmailAddress.clear();
		createAccountEmailAddress.sendKeys(createEmailAddress);
	}

	public void clickOnCreateAccountButton() {
		createAccountButton.click();
	}

	public String getValidationMessage() throws InterruptedException {
		Thread.sleep(3000);
		return createAccountValidationMsg.getText();

	}

	public void clickOnRegisterForFreeButton() {
		registerForFreeButton.click();
	}

	public boolean verifyIncorrectUsernamePasswordErrorisDisplayed() throws InterruptedException{
		return incorrectUsernamePasswordError.isDisplayed();
	}

	public boolean verifyEmailFieldErrorIsDisplayed() throws InterruptedException{
		return emailFieldErrorMessage.isDisplayed();
	}

	public String getEmailFieldErrorMessage() throws InterruptedException {
		Thread.sleep(3000);
		return emailFieldErrorMessage.getText();

	}

	public boolean verifyEmaildRequiredErrorIsDisplayed() throws InterruptedException{
		return emailRequiredFieldErrorMessage.isDisplayed();

	}

	public boolean verifyPasswordRequiredErrorIsDisplayed() throws InterruptedException{
		return passwordRequiredFieldErrorMessage.isDisplayed();
	}

}

