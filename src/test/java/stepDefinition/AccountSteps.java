package stepDefinition;

import com.lfc.pageObjects.AccountPage;
import com.lfc.pageObjects.HomePage;
import com.lfc.pageObjects.LoginPage;
import com.lfc.utils.BaseClass;
import com.lfc.utils.Log;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import static org.testng.Assert.assertTrue;

public class AccountSteps extends BaseClass{

    LoginPage loginPage = new LoginPage(driver);
    Log log= new Log();
    HomePage homePage= new HomePage(driver);
    AccountPage accountPage= new AccountPage(driver);

    @Then("^verify that user logged into application$")
    public void VerifyUserLoggedIn() throws Throwable {
        try {
            boolean signOutLink = accountPage.verifySignOutisDisplayed();
            assertTrue(signOutLink, "Sign Out link should be displayed");

            Log.info("Verify that signout link is displayed");
        }
        catch(Exception e) {
            Assert.fail("Unable to verify user is logged into the application., Exception: " + e.getMessage());
        }


    }

}
