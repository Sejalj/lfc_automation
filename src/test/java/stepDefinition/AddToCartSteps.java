package stepDefinition;

import com.lfc.pageObjects.AddToCartPage;
import com.lfc.pageObjects.HomePage;
import com.lfc.pageObjects.LoginPage;
import com.lfc.pageObjects.PLPPage;
import com.lfc.utils.BaseClass;
import com.lfc.utils.Log;
import com.lfc.utils.PropertyReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import static org.testng.Assert.assertTrue;

public class AddToCartSteps extends BaseClass {

    LoginPage loginPage = new LoginPage(driver);
    HomePage homePage= new HomePage(driver);
    PLPPage plpPage= new PLPPage(driver);
    AddToCartPage addToCartPage= new AddToCartPage(driver);
    public PropertyReader propertyReader = new PropertyReader();
    CommonSteps commonSD= new CommonSteps();



    @Given("^user logged-in into application using email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void User_on_login_screen(String username, String password) throws Throwable {
        try{
            homePage.closeCookieAlert();
            loginPage.loginIntoApplication(username, password);


            Log.info("user logged-in into application is successful");
        }
        catch(Exception e) {
            Assert.fail("user unable to log-in into application., Exception: " + e.getMessage());
        }

    }

    @And("^navigate to product url$")
    public void Navigate_to_product_url() throws Throwable {
        try{
            addToCartPage.navigationToProductURL();
            commonSD.takeScreenshot();

            Log.info("navigate to product url is successful");
        }
        catch(Exception e) {
            Assert.fail("Unable to navigate to product url, Exception: " + e.getMessage());
        }

    }

    @When("^select the product$")
    public void selectTheProduct() throws Throwable {

        //homePage.mouseHoverOnWomenNavigationBar();
        //	  homePage.clickOnTshirtSubMenu();
        plpPage.clickOnFadedShortTshirtLink();


    }

    @Then("^select product size$")
    public void Select_product_size() throws Throwable {
        try{
            addToCartPage.clickOnSize8Option();

            Log.info("select product size is successful");
        }
        catch(Exception e) {
            Assert.fail("Unable to select product size, Exception: " + e.getMessage());
        }

    }

    @And("^add product to bag$")
    public void Add_product_to_bag() throws Throwable {
        try{
            addToCartPage.clickOnAddToCartButton();
            plpPage.clickOnPopUpCloseButton();

            Log.info("add product to bag is successful");
        }
        catch(Exception e) {
            Assert.fail("Unable to add product to bag, Exception: " + e.getMessage());
        }
    }

    @Then("^verify the product is added to bag$")
    public void verify_the_product_is_added_to_bag() throws Throwable {
        try {
            Thread.sleep(3000);
            String expectedProductTitle = plpPage.getProductTitle();
            System.out.println("Product to be added in Bag is "+expectedProductTitle);
            plpPage.clickOnCartIcon();
            String actualTitle = addToCartPage.getProductTitleFromMiniCart();
            System.out.println("Product acutally added in Bag is "+actualTitle);
            commonSD.takeScreenshot();
            if (expectedProductTitle.equals(actualTitle)) {
                //	addToCartPage.clickOnRemoveButtonInMiniCart();
                //	addToCartPage.clickOnRemoveConfirmationOptionOk();
                Assert.assertTrue(true, "Product is successfully added to bag");
            }else {
                Assert.fail("Product is not added to bag");
            }

            Log.info("verify the product is added to bag is successful");
        }
        catch(Exception e) {
            Assert.fail("Product added to bag verification failed, Exception: " + e.getMessage());
        }

    }



    @And("^checkout the product$")
    public void checkoutProduct() throws Throwable {
        addToCartPage.clickOnAddToCartButton();
        addToCartPage.clickOnProceedToCheckoutButton();
        //addToCartPage.clickOnProceedToCheckoutButtonAtShoppingCartSummaryButton();
        addToCartPage.clickOnProceedToCheckoutButtonAtShoppingCartAddressButton();
        addToCartPage.checkTermsAndConditionCheckbox();
        addToCartPage.clickOnProceedToCheckoutButtonAtShoppingCartShippingButton();
        addToCartPage.clickOnBankWirePaymentButton();
    }

    @Then("^verify the payment confirmation$")
    public void verifyPaymentConfirmation() throws Throwable {

        addToCartPage.clickOnConfirmOrderButton();
        assertTrue(addToCartPage.getOrderConfirmationMessage().contains("Your order on My Store is complete"));

    }

}

