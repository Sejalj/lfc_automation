#Author: Sejal Jawale

Feature: Testing Invalid Login Feature

  @SmokeTest1
  Scenario: Verify the user is unable to login using invalid creds
    Given user is on login screen
    When enter email "User14012022130020@gmail.com" and password "Password@14012022020"
    Then verify that incorrect username and password error message is displayed

  @SmokeTest1
  Scenario: Verify email field error message when user enter special character
    Given user is on login screen
    When enter email "Test!@£$**" and password "4V4@J#PZf"
    Then verify that email field error message is displayed

  @SmokeTest1
  Scenario: Verify email and password field required error message
    Given user is on login screen
    And click on "Sign In" button
    Then verify that email and password required error message is displayed 